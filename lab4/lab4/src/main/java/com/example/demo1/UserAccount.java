package com.example.demo1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
//описание пользователя (класса UserAccount)
public class UserAccount {
   // характеристики пользователя (класса UserAccount)
   public static final String GENDER_MALE = "M";
   public static final String GENDER_FEMALE = "F";
   private String userName;
   private String gender;
   private String password;
   private List<String> roles;

   //конструктор по умолчанию (класса UserAccount)
   public UserAccount() {

   }

   //конструктор пользователя (класса UserAccount)
   public UserAccount(String userName, String password, String gender, String... roles) {
      this.userName = userName;
      this.password = password;
      this.gender = gender;

      this.roles = new ArrayList<String>();
      if (roles != null) {
         this.roles.addAll(Arrays.asList(roles));
      }
   }
// методы (get-гетеры и set-сетеры)
   public String getUserName() {
      return userName;
   }

   public void setUserName(String userName) {
      this.userName = userName;
   }

   public String getGender() {
      return gender;
   }

   public void setGender(String gender) {
      this.gender = gender;
   }

   public String getPassword() {
      return password;
   }

   public void setPassword(String password) {
      this.password = password;
   }

   public List<String> getRoles() {
      return roles;
   }

   public void setRoles(List<String> roles) {
      this.roles = roles;
   }
}