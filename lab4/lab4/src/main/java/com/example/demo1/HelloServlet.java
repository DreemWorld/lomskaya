package com.example.demo1;

import java.io.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

// Часть url ссылки по которой можно попасть в данных сервлет
@WebServlet("/helloWorld")
// Класс главной страницы (HelloServlet), порожденная от HttpServlet
public class HelloServlet extends HttpServlet {
    // Объявление полей класса главной страницы (HelloServlet)
    private String message;
    // Методы инициализации
    // Присваиваем переменной message строку "Hello Servlet!"
    @Override
    public void init() {
        message = "Hello Servlet!";
    }
    //метод отправки пользователю HTML страницы
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");
        out.println("</body></html>");
    }
}